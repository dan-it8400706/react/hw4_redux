import { Link } from "react-router-dom"
import RenderCard from "../renderCard/RenderCard"
import style from './ListSelected.module.scss'
import { addToCart } from "../../store/cartSlise.js"
// import { useEffect, useState } from "react"
import { useDispatch } from "react-redux"

function ListSelected(props){


    // console.log(props.cartSelected)
    // console.log(props.setCartSelected)

    // const OnClick = () => {

    //     const cart = JSON.parse(localStorage.getItem('cart'))
    //     props.setCartArray (c => {
    //         if (cart.find((p) => p.id === Object.id)){
    //             return alert("This ptoduct in cart")

    //             } else{
    //                 // console.log("else")
    //                 return [...cart, Object]
    //             }
    //         })
    // }

    // const SelectedControl= ()=>{
    //     const mySelect = JSON.parse(localStorage.getItem('cartSelected'))
    //     props.setCartSelected (c=>{
    //         if (mySelect.find((p) => p.id === Object.id)){
    //                     console.log('this card did selected')
    //         }else {
    //             return [...mySelect, Object]
    //         }
    //     })
    //     }

// const [mySelected, setMySelected]=useState(JSON.parse(localStorage.getItem('cartSelected'))|| [])
//     // let mySelected = JSON.parse(localStorage.getItem('cartSelected'))
// useEffect(()=>{
//     setMySelected(JSON.parse(localStorage.getItem('cartSelected')))
//     // localStorage.setItem('cartSelected', JSON.stringify('cartSelected'))
// },[])

    const dispatch = useDispatch();
    const addObj = (Product) => {
        // console.log(Product)
    dispatch(addToCart(Product));
    };

    return(
        <Link to="/selected">
            <ul className={style.ul}>
                {
                props.mySelected.map(p => 
                <RenderCard
                key={p.title+p.index} 
                name={p.title} id={p.id} 
                price={p.price} 
                title={p.title}
                image={p.image} 
                header="Додавання товару до кошика"
                text = "Ви бажаєте додати товар до кошику?"
                textButton='Add to cart' 
                backgroundButton='green'
                mySelected={props.mySelected} 
                setMySelected={props.setMySelected} 
                cartArray={props.cartArray} 
                setCartArray={props.setCartArray}
                onClick={()=>addObj(p)}
                // display= "None"
                // displaySecond = "block"
                />)}
            </ul>
        </Link>
                
    )
}
export default ListSelected