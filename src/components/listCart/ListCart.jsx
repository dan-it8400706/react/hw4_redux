import { Link } from "react-router-dom"
import style from './ListCart.module.scss'
import RenderCard from "../renderCard/RenderCard"
import {  removeFromCart } from '../../store/cartSlise.js'
import { useDispatch } from "react-redux"

function ListCart (props){

let myCart = JSON.parse(localStorage.getItem('cart'))


const dispatch = useDispatch();

const removeObj = () => {
    dispatch(removeFromCart());
};
    return(
        <Link to="/cart">
            <ul className={style.ul}>
                {myCart.map(p => 
                <RenderCard
                key={p.title+p.index} 
                name={p.title} id={p.id} 
                price={p.price} 
                title={p.title}
                image={p.image} 
                header="Видалення товару з кошика"
                text = "Ви бажаєте видалити товар з кошика?"
                textButton='Delete from cart'
                cartArray={props.cartArray} 
                setCartArray={props.setCartArray} 
                mySelected={props.mySelected}
                setMySelected={props.setMySelected}
                backgroundButton = "green"
                onClick = {removeObj}
                displayFirst = "None"
                displaySecond = "block"
                />)}
            </ul>
        </Link>

    )
}
export default ListCart