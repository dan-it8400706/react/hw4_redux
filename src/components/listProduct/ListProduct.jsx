import RenderCard from "../renderCard/RenderCard";
import { useState, useEffect } from "react";
import style from './ListProduct.module.scss'
import { addToCart } from "../../store/cartSlise";
import { useDispatch } from "react-redux";



function ListProduct (props){

  const [products, setProducts] = useState([])
  useEffect(()=>{
    const url='https://dummyjson.com/products?limit=10'
    fetch(url)
      .then(res => res.json())

      .then(data => {
      //   console.log(data)
        setProducts(data.products)
      });
    
  },[])
  // console.log(props.setCartSelected)

  
  const dispatch = useDispatch();
  const addObj = () => {
  dispatch(addToCart());
};



    return(
        <ul className={style.ul}>
            {products.map(p => <RenderCard 
            key={p.title+p.index} 
            title={p.title} 
            id={p.id} 
            price={p.price} 
            image={p.images[0]} 
            backgroundButton='green'
            header="Додавання товару до кошика"
            text = "Ви бажаєте додати товар до кошику?"
            cartArray={props.cartArray} 
            setCartArray={props.setCartArray}
            mySelected={props.mySelected} 
            setMySelected={props.setMySelected}
            textButton='Add to cart' 
            onClick={addObj}
            />)}
            {/* displayFirst = "block"
            displaySecond = "None" */}
        </ul>
    
    )
}

export default ListProduct