
import style from "./Modal.module.scss";
import Button from "../button/Button";
// import { render } from "react-dom";
// import { Context } from "../../Context";
// import { useState, useContext } from "react";
// import Cart from "../cart/Cart";


function Modal(props) {

    //console.log(props.cartArray)
    const Object = props.Object
    // console.log(Object)
    // console.log(props.setCartArray)

//  console.log(props.onClick)

    return (


        <div className={style.wrapper__modal} 
            onClick ={props.actions}>
            <div className={style.modal}>
                <h3 className={style.h3}>{props.header}
                    <span onClick={props.actions}>X</span>
                </h3>
                <p className={style.p}>{props.text}</p>
                <Button 
                    style={props.displayFirst}
                    background={props.background}
                    backgroundButton={props.backgroundButton}
                    onClick={props.onClick}
                    Object = {Object}
                    displayFirst = {props.displayFirst}
                    displaySecond = {props.displaySecond}
                    text={props.buttonFirstText}
                ></Button>
            </div>
        </div>
    )
}
export  default Modal