import { Link } from 'react-router-dom'
import Button from '../../button/Button'
// import ListSelected from '../../listSelected/ListSelected'


function Selected(){
    return(
        <Link to="/selected">
            <Button backgroundButton="none" color="black" text="Обране"/>
        </Link>
    )
}

export default Selected