import { Link } from "react-router-dom";
import Button from "../../button/Button";



function Home (){
    return(
        <Link to="/home">
            <Button backgroundButton="none" color="black" text="Home"/>
        </Link>
    )
}

export default Home