import style from './Header.module.scss'
import Home from './logo/Home';
import Selected from './selected/Selected';
import Cart from './cart/Cart';


function Header() {
    return ( 
        <header className={style.header}>
            <div className={style.wrapper}>
                <div className='logo'>
                    <Home text="Main"/>
                </div>
                <div className={style.cart}>
                    <Cart />
                    <Selected />
                </div>
            </div>
        </header>
    );
}

export default Header;