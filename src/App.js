import { BrowserRouter, Routes, Route } from "react-router-dom";
// import Counter from "./components/counter/Counter";
import style from './index.scss'
import Header from "./components/header/Header";
import ListCart from "./components/listCart/ListCart";
import ListProduct from "./components/listProduct/ListProduct";
import ListSelected from "./components/listSelected/ListSelected";
import { useState, useEffect } from "react";

function App() {

  //  кошик і все що з ни пов'язано

  const [cartArray,setCartArray]=useState(JSON.parse(localStorage.getItem('cart')) || [])

  useEffect(() => {
    localStorage.setItem('cart', JSON.stringify(cartArray))},[cartArray]);


//  Обране

  const [mySelected, setMySelected]=useState(JSON.parse(localStorage.getItem('mySelected'))|| [])
    useEffect(()=>{
      localStorage.setItem('mySelected', JSON.stringify(mySelected))},[mySelected]);
  return ( 

    <BrowserRouter>
    
      <Header />
      <main className={style.main}>
        <Routes>
            <Route path='/home' element={<ListProduct cartArray={cartArray} setCartArray={setCartArray} mySelected={mySelected} setMySelected={setMySelected}/>}/>
            <Route path='/selected' element={<ListSelected cartArray={cartArray} setCartArray={setCartArray} mySelected={mySelected} setMySelected={setMySelected}/>}/>
            <Route path='/cart' element={<ListCart cartArray={cartArray} setCartArray={setCartArray} mySelected={mySelected} setMySelected={setMySelected}/>}/> 
        </Routes>
      </main>
      {/* <Counter /> */}
    </BrowserRouter>

  );
}

export default App;