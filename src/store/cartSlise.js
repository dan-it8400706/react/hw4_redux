import { createSlice } from "@reduxjs/toolkit";

const initialState = JSON.parse(localStorage.getItem('cart')) || []

const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    addToCart: (state, action) => {
      const cart = JSON.parse(localStorage.getItem('cart')) ;
      console.log(cart);
      console.log(state)
      console.log(action.payload)
      cart ? localStorage.setItem('cart', JSON.stringify([...state, action.payload])) : localStorage.setItem('cart', JSON.stringify([action.payload]));
      state.push(action.payload);

    },
    removeFromCart: (state, action) => {
      // console.log('start remove')
      // const cart = JSON.parse(localStorage.getItem('cart'))
      // console.log(cart)
      // console.log(state)
      // state.cart = state.cart.filter((cart) => cart.id !== action.payload);
      // console.log('remove Finished')

      if (initialState){
        console.log(initialState.cart)
        state.cart = initialState.filter((cart) => {
          return cart.id !== action.payload;
        })
      }
        localStorage.setItem('cart', JSON.stringify([...state]))
      },
  },
});

export const { addToCart, removeFromCart} = cartSlice.actions;

export default cartSlice.reducer;