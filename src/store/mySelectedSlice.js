import { createSlice } from "@reduxjs/toolkit";

const initialState = JSON.parse(localStorage.getItem('mySelected')) || []

const mySelectedSlice = createSlice({
  name: "mySelected",
  initialState,
  reducers: {
    addToMySelected: (state, action) => {
      console.log('added to Selected')
      state.push(action.payload);
    },
    // delete: (state, action) => {
    //   state.tasks = state.tasks.filter((task) => task.id !== action.payload);
    // },
  },
});

export const { addToMySelected } = mySelectedSlice.actions;

export default mySelectedSlice.reducer;