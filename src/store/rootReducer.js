import { combineReducers } from "@reduxjs/toolkit";
import mySelectedSlice from  "./mySelectedSlice.js"
import cartSlise from "./cartSlise.js";


const rootReducer = combineReducers({
    mySelected : mySelectedSlice,
    cart : cartSlise,
})

export default rootReducer